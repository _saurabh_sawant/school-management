package org.dnyanyog.controller;

import org.dnyanyog.dto.request.AdmissionRequest;
import org.dnyanyog.dto.response.AdmissionResponse;
import org.dnyanyog.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

	@Autowired
	StudentService service;
	
	@PostMapping(path = "student/api/v1/admission",produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, consumes={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<AdmissionResponse>admission(@RequestBody AdmissionRequest request){
	
		return service.admission(request);
	}
}
