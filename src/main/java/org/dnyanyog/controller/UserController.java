package org.dnyanyog.controller;

import org.dnyanyog.dto.request.LoginRequest;
import org.dnyanyog.dto.request.RegistrationRequest;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.dto.response.RegistrationResponse;
import org.dnyanyog.service.UserService;
import org.dnyanyog.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@Autowired
	UserService service;
	@Autowired
	AuthenticationManager aauthenticationManager;
	@Autowired
	JwtUtil jwtUtil;

	private String token;
	
	@PostMapping(path = "user/api/v1/registration",produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, consumes={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<RegistrationResponse>registration(@RequestBody RegistrationRequest request){
	
		return service.registration(request);
	}
	
	@PostMapping(path = "user/api/v1/login",produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, consumes={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<LoginResponse>login(@RequestBody LoginRequest request){
		
		return service.login(request);
	}
	
	@GetMapping("/")
    public String welcome() {
        return service.welcome();
    }
	
//	@PostMapping("/authenticate")
//    public String generateToken(@RequestBody LoginRequest authRequest) throws Exception {
//        try {
//            aauthenticationManager.authenticate(
//                    new UsernamePasswordAuthenticationToken(authRequest.getMobileNumber(), authRequest.getPasscode())
//            );
//        } catch (Exception ex) {
//        
//        	//throw new Exception("inavalid username/password");
//        	return ("inavalid username/password");
//        }
//        token= jwtUtil.generateToken(authRequest.getMobileNumber());
//        return token;
//    }
}
