package org.dnyanyog.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.dnyanyog.config.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.function.Function;

@Service
public class JwtUtil {

	@Autowired
	SecurityConfig config;
    private String secret = "secret";

    public Date exp;
    public Date renew;
    public String userName;
    
    public String extractUsername(String token) {
    	userName = extractClaim(token, Claims::getSubject);
    	System.out.println("***userName***"+userName);
    	return userName;
    }

    public Date extractExpiration(String token) {
    	
    	
    	exp=extractClaim(token, Claims::getExpiration);
    	if(null==renew ) {
    		
//    		|| (new Date(System.currentTimeMillis()).after(renew))
    		renew = new Date(System.currentTimeMillis() + (config.getJwtExpiryTimeInMinutes()* 60*1000));
    	}
    	System.out.println(new Date(System.currentTimeMillis()));
    	System.out.println("***Renew***"+renew);
    	System.out.println("***Exp***"+exp);
    	return exp;
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
   
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(String username) {

        return Jwts.builder().setSubject(username).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + (config.getJwtExpiryTimeInMinutes() * 60 * 1000)))
                .signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}

