package org.dnyanyog.repository;

import org.dnyanyog.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, String>{

	Student findByParentMobileNumber(String parentMobileNumber);
	
	Student findByStudentMobileNumber(String studentMobileNumber);
}
