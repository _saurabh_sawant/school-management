package org.dnyanyog.service;

import org.dnyanyog.dto.request.AdmissionRequest;
import org.dnyanyog.dto.response.AdmissionData;
import org.dnyanyog.dto.response.AdmissionResponse;
import org.dnyanyog.entity.Student;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.StudentRepository;
import org.dnyanyog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

	@Autowired
	UserRepository userRepo;
	@Autowired
	StudentRepository studentRepo;
	@Autowired
	Users user;
	@Autowired
	Student student;
	@Autowired
	AdmissionResponse admissionResponse; 
	
	public ResponseEntity<AdmissionResponse> admission(AdmissionRequest request){
		
		admissionResponse = new AdmissionResponse();
		admissionResponse.setData(new AdmissionData());
		student = new Student();
		
		if (null != studentRepo.findByParentMobileNumber(request.getParentMobileNumber()))
			return getConflictAdmissionResponse();
					
		if (null != studentRepo.findByStudentMobileNumber(request.getStudentMobileNumber()))
			return getConflictAdmissionResponse();
		
		student.setStudentFirstName(request.getStudentFirstName());
		student.setFatherFirstName(request.getFatherFirstName());
		student.setMotherFirstName(request.getMotherFirstName());
		student.setLastName(request.getLastName());
		student.setParentMobileNumber(request.getParentMobileNumber());
		student.setAddress(request.getAddress());
		student.setStudentBirthDate(request.getStudentBirthDate());
		student.setGender(request.getGender());
		student.setApplyClass(request.getApplyClass());
		student.setPreviousSchoolName(request.getPreviousSchoolName());
		student.setStudentMobileNumber(request.getStudentMobileNumber());
		student.setEmail(request.getEmail());
		
		student = studentRepo.save(student);
		
		admissionResponse.setStatus("Success");
		admissionResponse.setMessage("User Registered Successfully");
		admissionResponse.getData().setGrnNumber(student.getGrnNumber());
		admissionResponse.getData().setStudentFirstName(student.getStudentFirstName());
		admissionResponse.getData().setFatherFirstName(student.getFatherFirstName());
		admissionResponse.getData().setMotherFirstName(student.getMotherFirstName());
		admissionResponse.getData().setLastName(student.getLastName());
		admissionResponse.getData().setParentMobileNumber(student.getParentMobileNumber());
		admissionResponse.getData().setAddress(student.getAddress());
		admissionResponse.getData().setStudentBirthDate(student.getStudentBirthDate());
		admissionResponse.getData().setGender(student.getGender());
		admissionResponse.getData().setApplyClass(student.getApplyClass());
		admissionResponse.getData().setPreviousSchoolName(student.getPreviousSchoolName());
		admissionResponse.getData().setStudentMobileNumber(student.getStudentMobileNumber());
		admissionResponse.getData().setEmail(student.getEmail());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(admissionResponse);
	}
	
	private ResponseEntity<AdmissionResponse> getConflictAdmissionResponse() {
		AdmissionResponse response = new AdmissionResponse();
		response.setStatus("Error");
		response.setMessage("Email or mobile number already registered");
		response.setData(null);

		return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
	}
}
