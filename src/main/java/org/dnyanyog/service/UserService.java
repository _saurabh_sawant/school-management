package org.dnyanyog.service;

import java.util.ArrayList;
import java.util.Date;
import java.time.*;

import org.dnyanyog.config.SecurityConfig;
import org.dnyanyog.dto.request.LoginRequest;
import org.dnyanyog.dto.request.RegistrationRequest;
import org.dnyanyog.dto.response.LoginData;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.dto.response.RegistrationData;
import org.dnyanyog.dto.response.RegistrationResponse;
import org.dnyanyog.entity.Users;
import org.dnyanyog.filter.JwtFilter;
import org.dnyanyog.repository.StudentRepository;
import org.dnyanyog.repository.UserRepository;
import org.dnyanyog.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	UserRepository userRepo;
	@Autowired
	StudentRepository studentRepo;
	@Autowired
	Users user;
	@Autowired
	RegistrationResponse registrationResponse;
	@Autowired
	LoginResponse loginResponse;
	@Autowired
	AuthenticationManager aauthenticationManager;
	@Autowired
	JwtUtil jwtUtil;
	@Autowired
	SecurityConfig config;
	
	private String token;

	public ResponseEntity<RegistrationResponse> registration(RegistrationRequest request) {

		registrationResponse = new RegistrationResponse();
		registrationResponse.setData(new RegistrationData());
		user = new Users();
		
		/*
		 * if (null == studentRepo.findByParentMobileNumber(request.getMobileNumber())
		 * && null == studentRepo.findByStudentMobileNumber(request.getMobileNumber()))
		 * return getConflictInvalidMobNoResponse();
		 */
		
		if (null != userRepo.findByMobileNumber(request.getMobileNumber()))
			return getConflictDuplicateMobNoResponse();
		
		user.setMobileNumber(request.getMobileNumber());
		user.setGrnNumber(request.getGrnNumber());
		user.setPasscode(request.getPasscode());
		if (null != studentRepo.findByParentMobileNumber(request.getMobileNumber())) {
			user.setRole("Parent");
		}
		
		 if (null != studentRepo.findByStudentMobileNumber(request.getMobileNumber())){
			 user.setRole("Student");
		}

		user = userRepo.save(user);

		registrationResponse.setStatus("Success");
		registrationResponse.setMessage("User Registered Successfully");
		registrationResponse.getData().setMobileNumber(user.getMobileNumber());
		registrationResponse.getData().setGrnNumber(user.getGrnNumber());
		registrationResponse.getData().setPasscode(user.getPasscode());
		registrationResponse.getData().setRole(user.getRole());

		return ResponseEntity.status(HttpStatus.CREATED).body(registrationResponse);
	}

	
	public ResponseEntity<LoginResponse> login(LoginRequest request) {

		loginResponse = new LoginResponse();
		loginResponse.setData(new LoginData());
		if (null != userRepo.findByMobileNumber(request.getMobileNumber())
				&& null != userRepo.findByPasscode(request.getPasscode())) {

			user.setMobileNumber(request.getMobileNumber());
			user.setPasscode(request.getPasscode());

			user = userRepo.findByMobileNumber(request.getMobileNumber());
			user = userRepo.findByPasscode(request.getPasscode());
			
			token= jwtUtil.generateToken(request.getMobileNumber());
			
			loginResponse.setStatus("Success");
			loginResponse.setMessage(token);
			loginResponse.getData().setMobileNumber(user.getMobileNumber());
			loginResponse.getData().setRole(user.getRole());

			return ResponseEntity.status(HttpStatus.CREATED).body(loginResponse);
		}

		loginResponse.setStatus("Error");
		loginResponse.setMessage("Validation Failed");
		loginResponse.getData().setMobileNumber("");
		loginResponse.getData().setRole("");
		return ResponseEntity.status(HttpStatus.CONFLICT).body(loginResponse);
	}
	
	public String welcome() {
		Date now = new Date(System.currentTimeMillis());
		System.out.println("***Now***"+now);
		System.out.println(jwtUtil.renew);
		System.out.println();
		if(now.after(jwtUtil.renew) && (now.before(jwtUtil.exp))) {
			jwtUtil.renew = new Date(System.currentTimeMillis() + (config.getJwtExpiryTimeInMinutes() *60*1000));
			token= jwtUtil.generateToken(jwtUtil.userName);
			return (token);
		}else {
			return ("Welcome");
		}
	}
	
	private ResponseEntity<RegistrationResponse> getConflictDuplicateMobNoResponse() {
		RegistrationResponse response = new RegistrationResponse();
		response.setStatus("Error");
		response.setMessage("Mobile Number already registered");
		response.setData(null);

		return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
	}
	
	private ResponseEntity<RegistrationResponse> getConflictInvalidMobNoResponse() {
		RegistrationResponse response = new RegistrationResponse();
		response.setStatus("Error");
		response.setMessage("Enter valid mobile number");
		response.setData(null);

		return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
	}


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		 Users user = userRepo.findByMobileNumber(username);
	        return new org.springframework.security.core.userdetails.User(user.getMobileNumber(), user.getPasscode(), new ArrayList<>());
	}
}
