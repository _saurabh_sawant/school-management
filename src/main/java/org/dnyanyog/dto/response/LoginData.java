package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class LoginData {

	private String mobileNumber;
	private String role;
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
}
